/* 
    Custom settings for datepicker and timepicker elements
 */
(function ($, document) {

// datepicker
    $('.datepicker-trigger').datepicker({
        firstDay: 1,
        monthNames: ["Leden", "�nor", "B�ezen", "Duben", "Kv�ten", "�erven", "�ervenec", "Srpen", "Z���", "��jen", "Listopad", "Prosinec"],
        dayNamesMin: ['Ne', 'Po', '�t', 'St', '�t', 'P�', 'So'],
        dateFormat: 'dd.mm.yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: "button",
        buttonText: "Date",
        onClose: function (val) {
            $(this).val(val);
            $(this).attr('value', val);
        },
        onSelect: function (val) {
            $(this).val(val);
            $(this).attr('value', val);
            $(this).change();
        }
    });

    $('input.datepicker-trigger[disabled]').datepicker('disable');


// datepicker + timepicker
    $.timepicker.regional['cs'] = {
        minuteText: 'Minuta',
        hourText: 'Hodina',
        timeText: '�as',
        currentText: 'Te�',
        closeText: 'OK'
    };
    $.timepicker.setDefaults($.timepicker.regional['cs']);

    $('.datepicker-timepicker-trigger').datetimepicker({
        firstDay: 1,
        monthNames: ["Leden", "�nor", "B�ezen", "Duben", "Kv�ten", "�erven", "�ervenec", "Srpen", "Z���", "��jen", "Listopad", "Prosinec"],
        dayNamesMin: ['Ne', 'Po', '�t', 'St', '�t', 'P�', 'So'],
        dateFormat: 'dd.mm.yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: "button",
        buttonText: "Date",
        onClose: function (val) {
            $(this).val(val);
            $(this).attr('value', val);
        },
        onSelect: function (val) {
            $(this).val(val);
            $(this).attr('value', val);
            $(this).change();
        }
    });

    $('input.datepicker-timepicker-trigger[disabled]').datetimepicker('disable');

// set default value to datepicker
    $('.datepicker-trigger').each(function () {
        var attrValue = $(this).attr('value');
        if (attrValue === "undefined") {
            $(this).datepicker('setDate', null);
        }
    });

// set default value to datepicker + timepicker
    $('.datepicker-timepicker-trigger').each(function () {
        var attrValue = $(this).attr('value');
        if (attrValue === "undefined") {
            $(this).datetimepicker('setDate', null);
        }
    });

    $(document).on("click", function (e) {
        var elee = $(e.target);

        if (!elee.hasClass('hasDatepicker') &&
                elee.parents('.ui-datepicker').length === 0 &&
                elee.parent().hasClass('ui-datepicker-header') === false) {

            $('.hasDatepicker').datepicker('hide');
        }

        e.stopPropagation();
    });

})(jQuery,document);